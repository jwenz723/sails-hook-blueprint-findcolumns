/**
 * Find Records
 * This takes the default findwhere blueprint and adds support for 
 * for specifying columns to select. Binds :model/findcolumns route for each RESTful model.
 *
 *  get   /:modelIdentity/findcolumns
 *
 * An API call to find and return model instances from the data adapter
 * using the specified criteria.  If an id was specified, just the instance
 * with that unique id will be returned.
 *
 * Optional:
 * @param {String} columns     - the columns that you want returned. Should be comma separated. (i.e. /Model?columns=column1,column2)
 * @param {Object} where       - the find criteria (passed directly to the ORM)
 * @param {Integer} limit      - the maximum number of records to send back (useful for pagination)
 * @param {Integer} skip       - the number of records to skip (useful for pagination)
 * @param {String} sort        - the order of returned records, e.g. `name ASC` or `age DESC`
 * @param {String} callback    - default jsonp callback param (i.e. the name of the js function returned)
 */

var _ = require('lodash');
var actionUtil = require('./actionUtil');
var pluralize = require('pluralize');

const findColumnsBlueprint = function(req, res) {
  // Look up the model
  var Model = actionUtil.parseModel(req);

  // Parse the columns that were specified by the user in the "columns" parameter of the URL.
  // If no columns were specified, use all columns.
  var columns = req.param('columns') ? {select: req.param('columns').replace(/ /g, '').split(',')} : {};

  // Lookup for records that match the specified criteria
  var query = Model.find(columns)
  .where( actionUtil.parseCriteria(req) )
  .limit( actionUtil.parseLimit(req) )
  .skip( actionUtil.parseSkip(req) )
  .sort( actionUtil.parseSort(req) );
  query = actionUtil.populateEach(query, req);
  query.exec(function found(err, matchingRecords) {
    if (err) return res.serverError(err);

    // Only `.watch()` for new instances of the model if
    // `autoWatch` is enabled.
    if (req._sails.hooks.pubsub && req.isSocket) {
      Model.subscribe(req, matchingRecords);
      if (req.options.autoWatch) { Model.watch(req); }
      // Also subscribe to instances of all associated models
      _.each(matchingRecords, function (record) {
        actionUtil.subscribeDeep(req, record);
      });
    }

    return res.ok(matchingRecords);
  });
};

module.exports = function (sails) {
  return {
      initialize: function(cb) {
        var config = sails.config.blueprints;
        var findcolumnsFn = findColumnsBlueprint;

        sails.on('router:before', function() {
          _.forEach(sails.models, function(model) {
            var controller = sails.middleware.controllers[model.identity];

            if (!controller) return;

            var baseRoute = [config.prefix, model.identity].join('/');

            if (config.pluralize && _.get(controller, '_config.pluralize', true)) {
              baseRoute = pluralize(baseRoute);
            }

            var route = baseRoute + '/findcolumns';

            sails.router.bind(route, findcolumnsFn, null, {controller: model.identity});
          });
        });

        cb();
      }
  }
};

